package model

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/bedrockruntime"
	gomock "github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestInvokeJurassic(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	client := NewMockBedrockRuntimeAPI(ctrl)
	mockResponse := "Hi, I am AI."
	mockResult := `{
		"completions": [
			{
				"data": {
					"text": "%s"
				}
			}
		]
	}`
	mockResult = fmt.Sprintf(mockResult, mockResponse)
	client.
		EXPECT().InvokeModel(gomock.Any()).
		Return(&bedrockruntime.InvokeModelOutput{
			Body:        []byte(mockResult),
			ContentType: aws.String("application/json"),
		}, nil).
		AnyTimes()

	response, _ := InvokeJurassic2(client, "Who are you?")
	assert.Equal(t, response, mockResponse)
}
